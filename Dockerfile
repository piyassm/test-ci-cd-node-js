FROM node:12-alpine

WORKDIR /usr/app

# RUN npm install --global pm2

COPY . .
RUN npm install
# RUN npm run build

EXPOSE 8081

# USER node

CMD [ "node", "server.js" ]